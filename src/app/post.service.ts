import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpClientModule } from '@angular/common/http';  
import { map, catchError } from 'rxjs/operators';
import { Post } from './interfaces/post';
import { User } from './interfaces/user';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  URLP:string ="https://jsonplaceholder.typicode.com/posts/";
  URLU:string = "https://jsonplaceholder.typicode.com/users/";


  constructor(private http:HttpClient, private db:AngularFirestore) { }
  
  getPosts(){
    return this.http.get<Post[]>(this.URLP);
    
  }
  
  getUsers(){
    return this.http.get<User[]>(this.URLU);
    
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error)
  }
 
  


}
