import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router'; 
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-authorform',
  templateUrl: './authorform.component.html',
  styleUrls: ['./authorform.component.css']
})
export class AuthorformComponent implements OnInit {
  author: string;
  id: number;
  isEdit:boolean = false;
  pagetitle:string = "Add";

  constructor(private router:Router , private route:ActivatedRoute, private bookservice:BookService) { }

  ngOnInit()
  {
    if(this.route.snapshot.params.id != null)
    {
      this.isEdit = true;
      this.pagetitle = "Update";
      this.id = this.route.snapshot.params.id;
    }
  }

  onSubmit(){
    if(this.isEdit)
    {
      this.router.navigate(['/authors',this.author,this.id]);
    }
    
    else { this.bookservice.addAuthor(this.author);
      this.router.navigate(['/authors']);    
    }
  }

}
