import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router'; 
import { BookService } from '../book.service';
import { Observable, of } from 'rxjs';


@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  authors$:Observable<any>;
  authors;
  updatedname: string;
  id;

  constructor(private router:Router , private route:ActivatedRoute, private bookservice:BookService) { }

  ngOnInit() {
    this.updatedname = this.route.snapshot.params.name;
    this.authors$ = this.bookservice.getauthors();
    this.id= this.route.snapshot.params.id;

  }

  onSubmit(){
       
     }
    
   editfun(id:number){
    this.router.navigate(['/authorform', id]);
   }

}
