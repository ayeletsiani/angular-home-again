import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  authors: object[] = [{id:1, name:'BoLewis Carrolts'}, {id:2, name:'Leo Tolstoy'},{id:3, name: 'Thomas Mann'}];
  id:number=5;
  constructor() { }

  getauthors(){
    console.log("getBooks");
    const authorObservable = new Observable
    (
        observer => 
                    observer.next(this.authors)
    )
    return authorObservable;
  }

  addAuthor(author:string){
    this.id++;
    this.authors.push({id:this.id ,name:author});
  }

}
