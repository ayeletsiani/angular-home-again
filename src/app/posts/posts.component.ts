import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { Observable } from 'rxjs';
import { Post } from '../interfaces/post';
import { User } from '../interfaces/user';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts$:Post[]=[];
  users$:User[]=[];
  success:string="";
  postCollection:AngularFirestoreCollection;
  
  constructor(private postservice:PostService, private db:AngularFirestore) { }

  ngOnInit() {
    
    this.postservice.getUsers()
         .subscribe(data =>this.users$ = data );
         console.log(this.postservice[3]);
    this.postservice.getPosts()
         .subscribe(data =>this.posts$ = data );
        
  }

  saveToFire()
  {
    for(let i=0; i<this.posts$.length; i++)
    {
      for(let j=0; j<this.users$.length;j++ )
      { 
        if(this.posts$[i].userId==this.users$[j].id)
        {
        const fullPost = {title:this.posts$[i].title,author:this.users$[j].name,body:this.posts$[i].title}
        this.db.collection('posts').add(fullPost);
        }
        this.success = "Save to Firebase successfully!"
        }
    
    }
  }
}
